# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/azurerm" {
  version     = "2.61.0"
  constraints = "~> 2.61.0"
  hashes = [
    "h1:efJD4eyFEhALYYKn1tS6v26shkJdf0d2kF1E87dDuRM=",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version     = "3.1.0"
  constraints = ">= 3.1.0"
  hashes = [
    "h1:XTU9f6sGMZHOT8r/+LWCz2BZOPH127FBTPjMMEAAu1U=",
  ]
}
