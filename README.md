### Usage Instructions

1. Clone repository.

2. Install the Azure CLI for your respective operating system: 
   https://docs.microsoft.com/en-us/cli/azure/install-azure-cli.

3. Install Terraform for your respective operating system:
   https://learn.hashicorp.com/tutorials/terraform/install-cli.

4. Move into the repository ``` cd <repository-name> ```.

5. Using your CLI, run ``` az login ``` and authenticate with your Azure account.

6. Run ``` terraform init ```

7. Run ``` terraform plan ```

8. Run ``` terraform apply -auto-approve ```

9. Variables to authenticate with your resources will be output.

10. Visit <public_ip>.

11. To SSH into the virtual machine instance

    ``` echo "<tls_private_key>" > tls_private_key.pem && chmod 600 <private-key-name>.pem ```

    ``` ssh <admin_username>@<public_ip> -i ./<private-key-name>.pem ```

12. You're done!
