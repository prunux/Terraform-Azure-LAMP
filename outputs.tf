output "vm_tls_private_key" {
  value       = tls_private_key.workshop-example_ssh.private_key_pem
  description = "Virtual machine admin private key"
  sensitive   = true
}
output "vm_admin_username" {
  value       = azurerm_linux_virtual_machine.workshop-example.admin_username
  description = "Virtual machine admin username"
}
output "public_ip" {
  value       = azurerm_public_ip.workshop-example.ip_address
  description = "Virtual machine public ip address"
}
output "mysql_admin_login_username" {
  value       = azurerm_mysql_server.workshop-example.administrator_login
  description = "Azure MySQL Server administrator login"
}
output "mysql_admin_login_password" {
  value       = azurerm_mysql_server.workshop-example.administrator_login_password
  description = "Azure MySQL Server administrator login password"
  sensitive   = true
}