terraform {
  required_providers {
    azurerm = {
      version = "~> 2.61.0"
    }
    tls = {
      version = ">= 3.1.0"
    }
  }
}


provider "azurerm" {
  features {}
}

provider "tls" {
}